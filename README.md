# Challenges pour la sélection de la délégation ULaval des CS Games 2018

## Introduction
Bonjour, si tu lis ce message, c'est que tu as eu le courage et la détermination d'être venu ici et d'au moins avoir lu les deux lignes au-dessus.

Les CS Games sont une des plus grandes compétitions d'informatique en Amérique du Nord et on a besoin de TOI pour gagner! On veut que tu puisses briller par tes connaissances, par tes skills en informatiques, mais surtout par ta volonté de vouloir apprendre et te dépasser.

Au terme de cet événement, on va pouvoir nommer les 10 personnes qui seront élues à participer à la première délégation ULaval pour les CS Games 2018 (et possiblement une deuxième délégation si on le calibre est au rendez-vous)!

#### L'événement
* 11 novembre 2017
* Le repository devient public à 13h et le dernier commit sur votre fork doit être fait avant 17h
* On fête ça au Pub Universitaire après!

## Instructions
* Faites un fork du repository et clonez-le sur votre machine.
* Dans le repository se trouve 14 sous-dossiers qui correspondent aux 14 défis.
* Chaque sous-dossier contient un README qui explique les instructions pour le défi en question.
* Vous devez répondre aux défis dans votre fork dans le bon sous-dossiers.
* N'oubliez-pas de commiter souvent et d'ajouter vos fichiers au repository de votre projet forké `git add --all`! On corrige votre fork selon son état dans le dernier commit qui a été fait avant 17h.
* Vous avez droit à tout, tous les coups sont permis.

#### Comment va-t-on déterminer les gagnants?
Chaque défi a un nombre de points attribués à l'avance (compris entre 1 et 5 points) par rapport à sa complexité. Cependant, cette pondération ne sera pas révélée puisqu'on ne veut pas que ça vous influence dans vos choix de défis. On veut que vous complétiez vraiment ceux dont le domaine vous intéresse ou que vous appréciez personnellement. On va prioriser les meilleurs scores totaux, mais on va aussi aller chercher les meilleurs de chaque catégorie afin d'avoir une délégation pluridisciplinaire.

Le pointage de certains défis spéciaux est donné! À vous d'en tirer avantage:

## But
Il y a probalement plus de défis proposés que vous serez capable d'en réussir en seulement 4h. On veut trouver dans quoi vous êtes les meilleurs. Commencez par faire les défis dans lesquels vous avez le plus d'affinités. Si vous n'êtes à l'aise dans aucune catégories, vous pouvez toujours essayer un domaine qui vous intéresse et chercher des informations sur internet! Le but du test de sélection est de former la meilleure équipe possible en ayant des gens dans chaque catégorie. On veut non seulement les meilleurs, mais aussi les plus motivés!
