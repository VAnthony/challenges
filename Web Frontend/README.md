# Web Frontend

## QR Proxy

### Introduction
Pour réussir ce challenge, vous devez créer une application web avec plusieurs fonctionnalités.
* Il y a trois pages à faire et leurs routes sont:
   * /
   * /qr
   * /frame
* Toutes les librairies sont permises.
* L'app doit être la plus esthétique possible.
* L'app doit s'installer avec la commande `npm install`
* L'app doit démarrer sur localhost:8080/ avec la commande `npm start`
* L'utilisation des frameworks frontend est fortement encouragée!

#### Template de base
Quand on navigue sur le site, peu importe la page, on doit avoir une nav bar qui reste au sommet de la page au-dessus de tout et qui indique la page active. Dans cette barre il y a des onglets pour chaque page navigable. Quand on clique sur un onglet, on tombe automatiquement sur la page associée. On veut un feedback visuel pour spécifier sur quelle page nous sommes.

On ne veut pas de duplication de code pour la nav bar entre les pages, on veut qu'elle soit dynamique.

Exemple: On est sur la page /qr, alors l'onglet "qr" de la nav bar prend une teinte plus pâle pour signifier qu'il est sélectionné.

#### Page d'accueil /
* La nav bar apparaît en haut.
* L'onglet accueil est sélectionné dans la nav bar.
* Dans cette page, on retrouve votre photo, votre nom, votre programme et un meme de qualité.
* Le meme doit être centré au milieu de la page.

###### Exemple
![](mockups/exemple_page_accueil.png)

#### Page /qr
Sur /qr, au centre on a un input dans lequel on peut entrer du texte.
* La nav bar apparaît en haut
* l'onglet qr est sélectionné dans la nav bar
* Lorsqu'on entre l'url d'un site web valide dans l'input, on a un feedback visuel.
  * Exemple: J'écris www.google.ca, et google.ca est actif, alors la barre d'input devient verte.
* Lorsqu'on entre l'url d'un site web non valide ou down, on a un feedback visuel.
* La chaîne de caractères dans la barre d'input doit rester présente même si on change de page et qu'on revient sur /qr.
* Si l'url est valide un bouton apparait.
* Lorsqu'on clique sur le bouton, on veut une animation chouette qui fait apparaître un QR code dans lequel est encodé l'url.
    * Exemple: J'écris www.google.ca dans la barre, google.ca est up, la barre devient verte, le bouton "FLIP" apparaît, en cliquant sur "FLIP", un carré au centre de l'écran a une animation qui lui donne l'effet de se retourner sur lui-même et dans son dos on retrouve le QR code.
* Le QR Code doit apparaître assez gros pour qu'il soit possible de le scanner avec son téléphone et tomber directement sur la page web validée.

###### Indices
* Documentation sur l'API de google pour générer des QR Codes: https://developers.google.com/chart/infographics/docs/qr_codes
  * Exemple de requête à l'API: GET https://chart.googleapis.com/chart?cht=qr&chl=VOTRE-URL-ICI&chs=200x200&chld=L|0
* Plus l'animation est complexe, plus j'aime ça.
* Pour valider si un site est actif, vérifiez dynamiquement si vous obtenez un status code de 200 quand vous lui envoyez une requête à l'url chaque fois qu'un nouveau caractère est ajouté.

###### Exemple
![](mockups/exemple_page_qr.png)

#### Page /frame
Sur /frame, on veut pouvoir visualiser le site web validé par la page qr.
* La nav bar apparaît en haut
* L'onglet frame est sélectionné dans la nav bar
* Si la page /qr contient dans la barre d'input un url valide, alors dans /frame on peut aller directement naviguer dans ce site.
* Si la page /qr ne contient pas d'url ou celle-ci est invalide, alors la page est vide.

###### Indices
* Utiliser un iframe pour afficher le site
* Garder l'url dans la barre d'input en mémoire globalement dans l'app

###### Exemple
![](mockups/exemple_page_frame.png)
