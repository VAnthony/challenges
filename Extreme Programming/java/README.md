# Extreme Programming

## The Bloop Goop Poop Challenge

### Pré-requis
* Avoir Java jdk 1.8+ installé
* Avoir maven installé

### Instructions
* `mvn clean install` pour installer les dépendances du projet
* `mvn test` pour rouler les tests uniquement

### But
Vous devez réussir ce challenge en **une seule ligne de code**.

Vous avez une classe qui s'appelle `BloopGoopPooper` qui possède une méthode `bloopPoopize`. Cette méthode prend en paramètre une liste d'entiers et c'est dans cette méthode que vous devrez insérer votre ligne de code. BloopPoopize doit retourner un dictionnaire en Java (`HashMap<Integer, String>`) comportant plusieurs entrées correspondant aux éléments de la liste. Les entrées ont comme clé un entier de la liste et comme valeur un des choix suivant:
* Si l'entier est un multiple de 3: `"bloop"`
* Si l'entier est un multiple de 5: `"poop"`
* Si l'entier est un nombre premier: `"goop"`

On peut combiner les catégories:
* Si l'entier est un multiple à la fois de 3 et de 5: `"bloop-poop"`
* Si l'entier est un multiple de 3 et est premier: `"bloop-goop"`
* Si l'entier est un multiple de 5 et est premier: `"poop-goop"`
* Si l'entier est un multiple de 3, un multiple de 5 et est premier: `"bloop-goop-poop"`. Le premier participant qui trouve un bloop-goop-poop est **automatiquement admis dans la délégation**! Dépêchez-vous à aller voir les organisateurs le cas échéant.

Si aucune de ces conditions sont remplies pour un entier, on n'ajoute pas d'entrées dans la HashMap pour cet entier.

### Conditions de réussite
* Tous les tests passent.
* Une instruction sur une seule ligne dans la méthode bloopPoopize.
* Vous ne pouvez pas ajouter d'autres fonctions à la classe.
* Vous ne pouvez pas créer de classes publiques.
* Vous pouvez créer des classes privées au besoin, mais elles ne doivent avoir aucune logique à l'intérieur, seulement des getters et setters.
* Vous ne pouvez pas modifier le code déjà présent dans les tests ni dans la classe BloopGoopPooper (sauf la ligne 9 évidemment)

### Indice
* Tirez profit de l'utilisation des Streams de Java 8 afin de chainer vos instructions en une ligne.
* Ça compte comme une seule ligne même si votre chaine déborde sur plusieurs lignes. En autant qu'il n'y ait qu'un seul `;`
* Si vous n'êtes pas certain de réussir ce challenge, vous pouvez miser sur vos capacités à trouver un bloop-goop-poop pour être automatiquement admis.


### Exemple
```
bloopGoopPooper.bloopPoopize([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

retourne le dictionnaire {(clé, valeur), } suivant:
{
  (2, "goop"),        --> nombre premier
  (3, "bloop-goop"),  --> nombre premier et multiple de 3
  (5, "poop-goop"),   --> nombre premier et multiple de 5
  (6, "bloop"),       --> multiple de 3
  (7, "goop"),        --> nombre premier
  (9, "bloop"),       --> multiple de 3
  (10, "poop")        --> multiple de 5
}

1, 4 et 8 sont retirés, car ils ne correspondent à aucun critère.
```
