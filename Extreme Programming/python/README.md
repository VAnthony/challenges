# Extreme Programming

## Un développeur doit toujours avoir les bons outils

### Pré-requis
Avoir python 3.5+ installé.

### Instruction
* `python main.py` pour exécuter le module python
* Pour réussir le défi vous devez créer les deux fonctions suivantes dans le fichier `main.py`:
    * `shield_decorator`
    * `sword_decorator`
* Vous ne devez pas modifier le main ni la fonction `stickman()`
* Vous avez le droit d'importer des packages standard

### But
Vous avez une fonction de base `stickman()` qui retourne un petit personnage en ascii art. Vous devez créer deux décorateurs qui ajoutent les fonctionnalités suivantes:
* `shield_decorator` ajoute un bouclier au personnage en ascii art
* `sword_decorator` ajoute une épée au personnage en ascii art

Une fois ces décorateurs créés, vous devez aussi ajouter les annotations au-dessus de la fonction stickman.

En exécutant `print(stickman())` **sans les décorateurs**, on obtient:  
```

      O
      |  
    _/|\_
      |  
     / \
   _/   \_
```

### Résultat

###### Annotation sword_decorator
En exécutant le code suivant (la fonction uniquement annotée par sword_decorator):
```
@sword_decorator
def stickman():
  return open('stickman.txt').read()

if __name__ == "__main__":
  print(stickman())
```
On devrait avoir l'output suivant:
```
\
 \    O
 _\|  |
   M_/|\_
      |
     / \
   _/   \_
```

###### Annotation shield_decorator
En exécutant le code suivant (la fonction uniquement annotée par shield_decorator):
```
@shield_decorator
def stickman():
  return open('stickman.txt').read()

if __name__ == "__main__":
  print(stickman())
```
On devrait avoir l'output suivant:
```

      O
      |  }
    _/|\_|}
      |  }
     / \
   _/   \_
```

###### Les deux annotations ensemble
En exécutant le code suivant:
```
@sword_decorator
@shield_decorator
def stickman():
  return open('stickman.txt').read()

if __name__ == "__main__":
  print(stickman())
```
On devrait avoir l'output suivant:
```
\
 \    O
 _\|  |  }
   M_/|\_|}
      |  }
     / \
   _/   \_
```
