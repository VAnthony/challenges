#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "VehicleLogger.h"

static struct VehicleLogger *logger;

/*

Insert any static functions you want here

*/

struct VehicleLogger *VehicleLogger_new(struct VehicleInterface *vehicleCommands,
                                        struct ReferencedObject *vehicleReferencedObject)
{
    struct ReferencedObject *newReferencedObject = ReferencedObject_new();
    FILE *newLogFile = fopen("log.txt", "a+");

    setbuf(newLogFile, NULL);
    fprintf(newLogFile, "\n\n------------ NEW EXECUTION ------------\n\n");

    struct VehicleLogger *pointer = malloc(sizeof(struct VehicleLogger));

    ReferencedObject_reference(vehicleReferencedObject);
    pointer->vehicleReferencedObject = vehicleReferencedObject;
    pointer->referencedObject = newReferencedObject;
    pointer->logFile = newLogFile;

    pointer->originalVehicleCommands = malloc(sizeof(struct VehicleInterface));
    pointer->actualVehicleCommands = vehicleCommands;

    pointer->originalVehicleCommands->startEngine = pointer->actualVehicleCommands->startEngine;
    pointer->originalVehicleCommands->stopEngine = pointer->actualVehicleCommands->stopEngine;
    pointer->originalVehicleCommands->setSpeed = pointer->actualVehicleCommands->setSpeed;
    pointer->originalVehicleCommands->steer = pointer->actualVehicleCommands->steer;
    pointer->originalVehicleCommands->setFlashers = pointer->actualVehicleCommands->setFlashers;
    pointer->originalVehicleCommands->getAmountOfFuelInL = pointer->actualVehicleCommands->getAmountOfFuelInL;

    /* assign them using the vehicleCommands parameter

    pointer->originalVehicleCommands;
    pointer->actualVehicleCommands;

    */

    //J'ai un pointeur harleyDavidson->vehicleCommands L'INTERFACE
    //J'ai un pointeur harleyDavidson->referencedObject LA REFERANCE DE L'OBJET

    logger = pointer;

    return pointer;
}

void VehicleLogger_delete(struct VehicleLogger *logger)
{
    stopLogging();

    if(ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(logger->referencedObject)) {
        fprintf(logger->logFile, "\n\n------------ END OF EXECUTION ------------\n\n");
        fclose(logger->logFile);

        if(ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(logger->vehicleReferencedObject)) {
            free(logger->vehicleReferencedObject);
        }
        free(logger->originalVehicleCommands);
        free(logger);
    }
}

void startLogging(void)
{
  logger->actualVehicleCommands->startEngine = &startEngine;
  logger->actualVehicleCommands->stopEngine = &stopEngine;
  logger->actualVehicleCommands->setSpeed = &setSpeed;
  logger->actualVehicleCommands->steer = &steer;
  logger->actualVehicleCommands->setFlashers = &setFlashers;
  logger->actualVehicleCommands->getAmountOfFuelInL = &getAmountOfFuelInL;
}

void stopLogging(void)
{
  logger->actualVehicleCommands->startEngine = logger->originalVehicleCommands->startEngine;
  logger->actualVehicleCommands->stopEngine = logger->originalVehicleCommands->stopEngine;
  logger->actualVehicleCommands->setSpeed = logger->originalVehicleCommands->setSpeed;
  logger->actualVehicleCommands->steer = logger->originalVehicleCommands->steer;
  logger->actualVehicleCommands->setFlashers = logger->originalVehicleCommands->setFlashers;
  logger->actualVehicleCommands->getAmountOfFuelInL = logger->originalVehicleCommands->getAmountOfFuelInL;
    /*
        Complete me: once called, calls to the Vehicle methods should work normally (no logging)
    */
}

void startEngine()
{
  logger->originalVehicleCommands->startEngine();
  fprintf(logger->logFile, "Engine started\n");
}

void stopEngine()
{
  logger->originalVehicleCommands->stopEngine();
  fprintf(logger->logFile, "Engine stopped\n");
}

void setSpeed(short speedInKmh)
{
  logger->originalVehicleCommands->setSpeed(speedInKmh);
  char buffer[1000];

  sprintf(buffer, "%d", speedInKmh);
  fprintf(logger->logFile, "Speed set to ");
  fprintf(logger->logFile, "%s", buffer);
  fprintf(logger->logFile, " km/h\n");
}

void steer(short angleInDegrees)
{
  logger->originalVehicleCommands->steer(angleInDegrees);
  char buffer[1000];

  sprintf(buffer, "%d", angleInDegrees);
  fprintf(logger->logFile, "Steered of ");
  fprintf(logger->logFile, "%s", buffer);
  fprintf(logger->logFile, "°\n");
}

void setFlashers(_Bool status)
{
  logger->originalVehicleCommands->setFlashers(status);
  if (status)
  {
    fprintf(logger->logFile, "Flashers turned on\n");
    return;
  }
  fprintf(logger->logFile, "Flashers turned off\n");
}

unsigned int getAmountOfFuelInL()
{
  const unsigned int amount = logger->originalVehicleCommands->getAmountOfFuelInL();
  char buffer[1000];

  sprintf(buffer, "%d", amount);
  fprintf(logger->logFile, "Amount of fuel remaining in tank: ");
  fprintf(logger->logFile, "%s", buffer);
  fprintf(logger->logFile, "L\n");
}
