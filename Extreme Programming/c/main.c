#include "HarleyDavidson.h"
#include "HondaCivic.h"
#include "VehicleLogger.h"

int main(int argc, char *argv[])
{
    struct HarleyDavidson *harleyDavidson = HarleyDavidson_new();
    struct HondaCivic *hondaCivic = HondaCivic_new();
    struct VehicleLogger *logger = VehicleLogger_new(&harleyDavidson->vehicleCommands, harleyDavidson->referencedObject);

    startLogging();

    HarleyDavidson_startEngine(harleyDavidson);
    HarleyDavidson_setSpeed(harleyDavidson, 75);
    HarleyDavidson_setFlashers(harleyDavidson, 1);
    HarleyDavidson_steer(harleyDavidson, 90);
    HarleyDavidson_getAmountOfFuelInL(harleyDavidson);
    HarleyDavidson_stopEngine(harleyDavidson);

    stopLogging();

    HarleyDavidson_startEngine(harleyDavidson);
    HarleyDavidson_setSpeed(harleyDavidson, 50);
    HarleyDavidson_setFlashers(harleyDavidson, 0);
    HarleyDavidson_steer(harleyDavidson, 30);
    HarleyDavidson_getAmountOfFuelInL(harleyDavidson);
    HarleyDavidson_stopEngine(harleyDavidson);

    VehicleLogger_delete(logger);
    logger = VehicleLogger_new(&hondaCivic->vehicleCommands, hondaCivic->referencedObject);

    startLogging();

    HondaCivic_startEngine(hondaCivic);
    HondaCivic_setSpeed(hondaCivic, 40);
    HondaCivic_setFlashers(hondaCivic, 0);
    HondaCivic_steer(hondaCivic, -10);
    HondaCivic_getAmountOfFuelInL(hondaCivic);
    HondaCivic_stopEngine(hondaCivic);

    stopLogging();

    HondaCivic_startEngine(hondaCivic);
    HondaCivic_setSpeed(hondaCivic, 130);
    HondaCivic_setFlashers(hondaCivic, 1);
    HondaCivic_steer(hondaCivic, 15);
    HondaCivic_getAmountOfFuelInL(hondaCivic);
    HondaCivic_stopEngine(hondaCivic);

    VehicleLogger_delete(logger);
    HarleyDavidson_delete(harleyDavidson);
    HondaCivic_delete(hondaCivic);

    return 0;
}
