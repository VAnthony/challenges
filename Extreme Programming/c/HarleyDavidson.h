#ifndef HARLEYDAVIDSON_H_
#define HARLEYDAVIDSON_H_

#include "ReferencedObject.h"
#include "VehicleInterface.h"

struct HarleyDavidson {
    struct ReferencedObject *referencedObject;
    struct VehicleInterface vehicleCommands;
};

struct HarleyDavidson *HarleyDavidson_new(void);
void HarleyDavidson_delete(struct HarleyDavidson *harleyDavidson);

void HarleyDavidson_startEngine(struct HarleyDavidson *harleyDavidson);
void HarleyDavidson_stopEngine(struct HarleyDavidson *harleyDavidson);
void HarleyDavidson_setSpeed(struct HarleyDavidson *harleyDavidson, short speedInKmh);
void HarleyDavidson_steer(struct HarleyDavidson *harleyDavidson, short angleInDegrees);
void HarleyDavidson_setFlashers(struct HarleyDavidson *harleyDavidson, _Bool status);
unsigned int HarleyDavidson_getAmountOfFuelInL(struct HarleyDavidson *harleyDavidson);

#endif
