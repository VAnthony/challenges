#include <stdlib.h>
#include <time.h>
#include "HondaCivic.h"

#define HONDACIVIC_FUEL_TANK_CAPACITY 45

static void startEngine(void) {/* Call the engine and start it */}
static void stopEngine(void) {/* Call the engine and stop it */}
static void setSpeed(short speedInKmh) {/* Call the wheels and set their speed to this value */}
static void steer(short angleInDegrees) {/* Call the wheels and change their orientation accordingly */}
static void setFlashers(_Bool status) {/* Call the flashers and change their state accordingly */}
static unsigned int getAmountOfFuelInL(void)
{
    // Call the fuel tank and ask for the amount of fuel it contains
    srand(time(NULL));
    return rand() % HONDACIVIC_FUEL_TANK_CAPACITY;
}

static struct VehicleInterface setDefaultVehiculeCommandsTargets(void)
{
    struct VehicleInterface vehicleCommands = {
        .startEngine = &startEngine,
        .stopEngine = &stopEngine,
        .setSpeed = &setSpeed,
        .steer = &steer,
        .setFlashers = &setFlashers,
        .getAmountOfFuelInL = &getAmountOfFuelInL,
    };
    return vehicleCommands;
}

struct HondaCivic *HondaCivic_new(void)
{
    struct ReferencedObject *newReferencedObject = ReferencedObject_new();
    struct VehicleInterface newVehicleCommands = setDefaultVehiculeCommandsTargets();

    struct HondaCivic *pointer = malloc(sizeof(struct HondaCivic));

    pointer->referencedObject = newReferencedObject;
    pointer->vehicleCommands = newVehicleCommands;

    return pointer;
}

void HondaCivic_delete(struct HondaCivic *hondaCivic)
{
    if(ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(hondaCivic->referencedObject)) {
        free(hondaCivic);
    }
}

void HondaCivic_startEngine(struct HondaCivic *hondaCivic)
{
    (*(hondaCivic->vehicleCommands.startEngine))();
}

void HondaCivic_stopEngine(struct HondaCivic *hondaCivic)
{
    (*(hondaCivic->vehicleCommands.stopEngine))();
}

void HondaCivic_setSpeed(struct HondaCivic *hondaCivic, short speedInKmh)
{
    (*(hondaCivic->vehicleCommands.setSpeed))(speedInKmh);
}

void HondaCivic_steer(struct HondaCivic *hondaCivic, short angleInDegrees)
{
    (*(hondaCivic->vehicleCommands.steer))(angleInDegrees);
}

void HondaCivic_setFlashers(struct HondaCivic *hondaCivic, _Bool status)
{
    (*(hondaCivic->vehicleCommands.setFlashers))(status);
}

unsigned int HondaCivic_getAmountOfFuelInL(struct HondaCivic *hondaCivic)
{
    return (*(hondaCivic->vehicleCommands.getAmountOfFuelInL))();
}

