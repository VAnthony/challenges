# OS

## Super quiz!

### Instruction
Vous devez répondre au questionnaire directement dans ce README en dessous de chaque question. Ces questions touchent plusieurs aspects de la programmation de bas niveau et les réponses sont généralement assez courtes.

###### Question 1
Quelle est la différence entre le heap et le stack (le monceau et la pile).

Le stack est une partie de la mémoire (RAM) utilisé pour l'allocation de mémoire statique.
Le heap est une partie de la mémoire (RAM) utilisé pour l'allocation de mémoire dynamique.

###### Question 2
Que fais la fonction malloc?

La fonction malloc permet d'allouer de la mémoire à partir de la partie dynamique de la mémoire (donc dans la HEAP).
La fonction malloc peut donc réserver un emplacement mémoire dans la HEAP.

###### Question 3
Décrivez le fonctionnement de ce code.

```
uint16_t lastKeys = 0;
uint16_t diff, currentKeys;

while (1) {
	currentKeys = scanKeyboard();
	if ((diff = (uint16_t) (currentKeys & (~lastKeys))) || !currentKeys)
		lastKeys = currentKeys;
	faire_quelque_chose(diff);
}
```

La variable lastKeys est assignée à la valeur 0
Le code suivant est exécuté indéfiniement :
	- La touche courante est récupérée à l'aide de la fonction scanKeayBoard() et stockée dans la variable currentKeys
	- L'opérateur binaire & est exécuté (copie des bits dans le résultat si le bit est présent chaque opérandes)
	- La variable diff est assigné au cast du résultat du calcul suivant : comparaison binaire & (copie des bits dans le résultat si le bit est présent chaque opérandes) entre la variable currentKeys et la variable lastKeys (avec ses bits retournées).
	- Si la variable diff ne contient pas 0, la variable lastKeys est assignè à la valeur de currentKeys
	- Sinon si currentKeys vaut 0, la variable lastKeys est assigné à la valeur de currentKeys
	- La fonction faire_quelque_chose est appellée avec l'argument diff

###### Question 4
Que signifie les 8 paramètres de cette commande GCC:  
`gcc -Os -Wall -g3 -flto -lpthread -o main main.c`

Le 1er paramètre indique que gcc va effectuer des optimisation sur la taille du code
Le 2ème paramètre indique que tout les avertissements (warning) de compilation sont activés
Le 3ème paramètre indique que les informations de deboquages sont activé au niveau 3 (niveau maximum)
Le 4ème paramètres indique que gcc va compiler avec la librairie dynamique pthread
Le 5ème paramètre indique que l'optimisation de l'éditeur de lien (linker) est activé
Le 6ème paramètre indique à gcc d'écrire le résultat de la compilation dans le fichier avec le nom spécifié à l'argument suivant.
Le 7ème paramètre indique le nom du fichier contenant le résultat de la compilation (exécutable)
Le 8ème paramètre indique le nom du fichier à compiler

###### Question 5
À quoi sert le DMA dans un microcontrolleur ou un ordinateur.

Le DMA (Direct Memory Access) permet de recevoir et d'envoyer des données directement vers ou de la RAM sans passé par le CPU.
Cela permet d'augmenter la vitesse des opérations mémoires.

###### Question 6
Quelle est la différence entre mutex et sémaphore?

Une mutex est destinée à protéger une ressource partagé entre plusieurs threads.
Une sémaphore permet d'organiser le fil d'exécution entre plusieurs threads. En effet, une sémaphore ne protège pas une ressource partagée mais permet de synchronisation entre plusieurs threads. Ainsi, certain thread vont attendre la fin de l'exécution d'autre threads.

###### Question 7
Un microcontrolleur lit, avec son convertisseur analogique à numérique à 12 bits, la température d'une cuve de fermentation à l'aide d'un thermomètre. Votre microcontrolleur fonctionne en 5V. Le thermomètre retourne une valeur entre 0 et 5V correspondant aux températures respectivement entre 15-25 degrées Celcius. Assumez que le comportement du thermomètre est linéaire.
Donnez la précision de lecture de votre microcontrolleur. Dans quel type de variable devriez-vous sauvegarder la valeur lue de façon à conserver le maximum de précision (char, int, long, float, double, etc), pourquoi? S'il y a lieu, expliquez aussi à quoi correspondent les valeurs enregistrées. (Max 10 lignes)


###### Question 8
Expliquer une façon de faire le "debounce" d'un bouton.


###### Question 9
Comment pourriez-vous faire pour mesurer la capacitance (en Farrad) d'un condensateur à l'aide d'un Arduino ?


###### Question 10
Que fait ce code et à quoi peut-il servir ? Existe-t-il un équivalent dans la librairie standard du C?

`while (*p++ = *q++) ;`

Ce code assigne la valeur pointé par le pointeur p à la valeur pointé par le pointeur q. Les 2 pointeurs sont ensuite incrémentés.
strcpy est un équivalent de la librairie standard du C.

###### Question 11
Décrivez les principales fonctions d'un microprocesseur.
